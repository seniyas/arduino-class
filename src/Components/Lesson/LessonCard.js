import React from 'react'
import downlaod from './download-arrow.svg'
import firebase from 'firebase'

class LessonCard extends React.Component {
    constructor(props) {
        super()
    }
    deleteLesson = () => {

        const { id } = this.props
        const db = firebase.firestore()
        db.collection("lessons").doc(id).delete()

    }

    deleteclicked = (e) => {
        e.preventDefault();
        this.deleteLesson()
    }
    render() {
        const { title, url, note, publishDate, route, classdate } = this.props
        if (route === "admin") {

            return (
                <div className='ma2 pb2 black-90 bl bw2 b--blue bg-white center w-80 w-100-l w-100-m br2 shadow-5'>

                    <div className="flex pl2 ">
                        <p className='f3 blue'>Lesson:{title}</p>
                        <a target="_blank" href={url} >
                            <img className='link pt3 ma2 dim' src={downlaod} width='25' height='25' alt="Downlaod the Code file bundle"></img>
                            <button className="pa2" onClick={this.deleteclicked}>Delete</button>
                        </a>
                    </div>


                    <p className='f5 pl2'>{note}</p>
                    <a className='f5 pl2 black-50'>Published on:{publishDate}</a>
                </div>
            )
        } else {

            if (classdate === "") {
                return (
                    <div className='ma2 pb2 black-90 bl bw2 b--blue bg-white center w-80 br2 shadow-5'>

                        <div className="flex pl2 ">
                            <p className='f3 blue'>Lesson:{title}</p>
                            <a className="pt4" target="_blank" href={url} >
                             <pre>More Info</pre>
                            </a>
                        </div>


                        <p className='f5 pl2'>{note}</p>
                        <a className='f5 pl2 black-50'>Published on:{publishDate}</a>
                    </div>
                )
            } else {
                return (
                    <div className='ma2 pb2 black-90 bl bw2 b--red bg-white center w-80 br2 shadow-5'>

                        <div className="flex pl2 ">
                            <p className='f3 blue'>Lesson:{title}</p>
                            <a target="_blank" href={url} >
                                <img className='link pt3 ma2 dim' src={downlaod} width='25' height='25' title="Downlaod the Code file bundle" alt="Downlaod the Code file bundle" ></img>
                            </a>

                        </div>
                        <div>
                            <p className='f5 pl2 ma3 '>{note}</p>
                            <a className='f5 pl2 black-50'>Published on:{publishDate}</a>
                            <a className="pl2 black">Class Date:{classdate}</a>

                        </div>
                    </div>
                )
            }
        }
    }

}
export default LessonCard;
