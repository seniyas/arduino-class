import React from 'react'
import Card from './LessonCard'
import firebase from 'firebase'

let lessons = []
let ids = []

class LessonList extends React.Component {
    componentWillMount() {
        lessons = []
        ids = []
        const db = firebase.firestore();
      db.collection('lessons').get()

            .then(collection => {
                collection.forEach(doc => {

                    lessons.push(doc.data())
                    ids.push(doc.id)
                   
                })
                this.setState({ lessons: lessons }) 
                // console.log(lessons)
                // console.log(ids)
            })
    }
    constructor(props) {
        super();
        this.state = {
            lessons: [],
            key:''
        }
    }
    Search = () => {

        const { searchText } = this.props
        const filteredlessons = this.state.lessons.filter(item => {
            return item.lesson.toLowerCase().includes(searchText.toLowerCase())
        })
        const filteredCardlist = filteredlessons.map((lesson, i) => {
            return <Card title={filteredlessons[i].lesson} url={filteredlessons[i].url} date={filteredlessons[i].date} note={filteredlessons[i].note} />
        });

        return filteredCardlist;
    }

    Loadcardlist = () => {

        //console.log(lessons);
        const cardlist = [];
        lessons.forEach((lesson,i) => {
         //   console.log(lesson.title)
            cardlist.push(<Card title={lesson.lesson} url={lesson.url} note={lesson.note} publishDate={lesson.publishDate} route={this.props.route} id={ids[i]} classdate={lesson.classdate}/>)
        });
      //  console.log("cardlist", cardlist)
        return cardlist;
    }

    render() {

        const { searchText } = this.props
        if (searchText !== '') {
            // console.log(filteredlessons)
            // console.log("Cards", filteredCardlist)
            return (
                <div >


                    {this.Search()}
                </div>
            )
        } else {



            // console.log(lessons)

            return (
                <div >
                    {this.Loadcardlist()}
                </div>
            )
        }

    }
}
export default LessonList;