import React from 'react';

import 'tachyons';
import * as firebase from 'firebase'
import logo from './ardu team logo_white.png'

var config = {
  apiKey: "GdzVHQ4E7pvx6R98h7BXLpcar1LRvdSKNcbJQ8c",
  authDomain: "arduino-class-e854a.firebaseapp.com",
  databaseURL: "https://arduino-class-e854a.firebaseio.com",
  projectId: "arduino-class-e854a",
  storageBucket: "arduino-class-e854a.appspot.com",
  messagingSenderId: "221258558000"
};

firebase.initializeApp(config);
class Signin extends React.Component {

  constructor(props) {
    super()

    this.state = {
      signinId: '',
      signinPassword: '',
      name: '',
      message: ''
    }
  }




  onIDChange = (event) => {
    this.setState({ signinId: event.target.value })
  }
  onPasswordChange = (event) => {
    this.setState({ signinPassword: event.target.value })
  }
  //if signin Succseed

  onSubmitSignin = (e) => {
    this.setState({ message: "Loading" })
    //this.props.setState({id:this.state.signinPassword})
    if (this.state.signinId !== "" || this.state.signinPassword !== "") {
      e.preventDefault();
      const db = firebase.firestore();
      db.settings({
        timestampsInSnapshots: true
      });

      const userRef = db.collection('users').doc(this.state.signinId).get()

        .then(doc => {
          if (!doc.exists) {
            this.setState({ message: 'Wrong ID' })
            //console.log("The ID you entered is not registered")
            if (this.state.signinId === "admin" && this.state.signinPassword === "arduteamadmin") {
              this.props.ChangeRoute("admin")
            }

          } else {
            if (doc.data().IsRegistered === true) {
              if (doc.data().password === this.state.signinPassword) {
                // this.props.ChangeRoute('home')
                const userData = {
                  name: doc.data().name,
                  id: this.state.signinId
                }
                console.log("okey")
                this.props.LoardUsers(userData)
              } else {
                this.setState({ message: 'Password or User ID is incorrect' })
              }
            }else{
              this.setState({ message: 'Your ID is not registered.Please click Register' })
            }

          }

        }).catch(err => {
          this.setState({ message: 'Something Went Wrong,Please try again' })
          console.log('Something Went wrong', err)
        })

    } else {
      e.preventDefault();
      this.setState({ message: "ID and the Password required" })
    }


    //
  }



  render() {
    const { ChangeRoute } = this.props;
    return (
      <div className=' tc pa3'>

        <div className=" w-100 mt5" >

          <img src={logo} width='250' height="150" ></img>
          <p className="f3 white  tc">Robotics Morning Class Student Portal</p>

        </div>
        <h1>{this.state.name}</h1>
        <main className=" pa3  w-25-l w-50-m bt b--gold bw2 bg-near-white br3 mb3 black-80 shadow-2 center m">
          <form className="measure center">
            <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
              <legend className="center f2 fw6 ph0 mh0">Sign In</legend>
              <div className='tc'>
                <a>{this.state.message}</a>
              </div>
              <div className="tc mt3">
                <input placeholder="ID" onChange={this.onIDChange} className="br2 pa2 input-reset ba bw1 maroon bg-transparent  w-50" type="text" name="id" id="id" />
              </div>
              <div className="mv3 tc">

                <input placeholder="Password" onChange={this.onPasswordChange} className="br2 pa2 input-reset ba bw1 maroon bg-transparent  w-50" type="password" name="password" id="password" />
              </div>

            </fieldset>
            <div className="tc">
              <input onClick={this.onSubmitSignin} className="b center ph3 pv2 input-reset ba b--gold bw1 bg-transparent br3 grow pointer f6 dib" type="submit" value="Sign in" />
            
            <br></br>

            <div className="pa3 pointer"> <a  onClick={()=>ChangeRoute("aboutus")}>Need Help?/About Us</a></div>
           
            </div>
          </form>
        </main>

      </div>
    )

  }
}
export default Signin;