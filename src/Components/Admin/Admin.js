import React from 'react'
import firebase from 'firebase'
import LessonList from '../Lesson/lessonlist';
import Scroll from '../Scroll/Scroll'
let usercount = ""
class Admin extends React.Component {
    constructor(props) {
        super();
        this.state = {
            inputclassDate: "",
            inputLesson: '',
            inputNote: '',
            inputUrl: '',
            inputNotice: '',
            message: '',
            message2: "",
            key: '',
            inputUserCount: ''
        }
    }

    OninputDateChange = (event) => {
        this.setState({
            inputclassDate: event.target.value,
            message: ''
        })
    }

    checkDate = () => {
        let exists = false;
        const db = firebase.firestore();
        const lessonRef = db.collection("lessons").doc(this.state.inputDate)
        const getDoc = lessonRef.get()
            .then(doc => {
                if (doc.exists) {
                    exists = true;
                }
            })

        return exists;
    }
    OninputLessonChange = (event) => {
        this.setState({
            inputLesson: event.target.value

        })
    }
    OninputNoteChange = (event) => {
        this.setState({
            inputNote: event.target.value
        })
    }
    OninputUrlChange = (event) => {
        this.setState({
            inputUrl: event.target.value
        })
    }
    uploadFile = () => {
        //  console.log("inputurl", this.state.inputUrl)
        const storage = firebase.storage();
        var storageRef = storage.ref()
        var spaceRef = storageRef.child("codefiles/" + this.state.file.name);
        spaceRef.put(this.state.file).then(function (snapshot) {
            console.log('File Uploaded');
        })

        setTimeout(() => {
            const storage2 = firebase.storage();
            var storageRef2 = storage2.ref()
            var spaceRef2 = storageRef2.child("codefiles/" + this.state.file.name);
            spaceRef2.getDownloadURL().then((url) => {
                this.setState({ inputUrl: url })

            }).then(() => {
               // console.log(this.state.inputUrl)
                const db = firebase.firestore();
                // console.log(this.checkDate())
                var today = new Date()
                const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + "| " + today.getHours() + ":" + today.getMinutes();

                const lessonRef = db.collection("lessons").doc().set({
                    classdate: this.state.inputclassDate,
                    lesson: this.state.inputLesson,
                    note: this.state.inputNote,
                    publishDate: date,
                    url: this.state.inputUrl
                })
                //  console.log("inputurl", this.state.inputUrl)

                this.setState({
                    message: "Sucessfully saved",
                    key: Math.random(),
                    inputclassDate: "",
                    inputLesson: '',
                    inputNote: '',
                    inputUrl: '',
                    inputNotice: ''
                })
            })
        }, 2000);


    }

    saveLesson = (e) => {
        e.preventDefault();
        let donwUrl = ""
        this.setState({ message: "Loading" })

        if (this.state.inputUrl === "") {
            this.uploadFile()
        } else {
            const db = firebase.firestore();
            // console.log(this.checkDate())
            var today = new Date()
            const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + "| " + today.getHours() + ":" + today.getMinutes();
         //   console.log(this.state.inputUrl)
            const lessonRef = db.collection("lessons").doc().set({
                classdate: this.state.inputclassDate,
                lesson: this.state.inputLesson,
                note: this.state.inputNote,
                publishDate: date,
                url: this.state.inputUrl
            }).then(() => {
                this.setState({
                    message: "Sucessfully saved",
                    key: Math.random(),
                    inputclassDate: "",
                    inputLesson: '',
                    inputNote: '',
                    inputUrl: '',
                    inputNotice: ''
                })
            })
            
        }
    }

    Refresh = () => {
        this.setState({ key: Math.random() })
    }
    OninputNoticeChange = (event) => {
        this.setState({ inputNotice: event.target.value })
    }
    saveNotice = () => {
        this.setState({ message2: '' })
        const db = firebase.firestore()
        db.collection("users").doc("notice").set({
            msg: this.state.inputNotice,

        })
        this.setState({ message2: "Saved!" })
    }
    onChangeFile(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
       // console.log(file);
        this.setState({ file });
    }
    OnUsercountChange = (event) => {
        // this.setState({ inputUserCount: event.target.value })
        usercount = event.target.value
    //    console.log(usercount)
    }
    addusers = (max) => {
        const userRef = firebase.firestore();
       // console.log("fgr")
        for (var i = 1; i < Number(max) + Number(1); i++) {
            userRef.collection("users").doc("19" + i).set({
                IsRegistered: false
            })
          //  console.log(i)
        }

    }


    render() {
        const { route } = this.props
        return (
            <div>
                <div className="pa3 bl b--blue bw2 bg-white br3 black-80 shadow-2 w-100 w-90-m w-80-l mt5 center flex flex-wrap" >
                    <main className="pa3 bt b--gold bw2 bg-near-white br3 mb3 black-80 shadow-2 w-100 w-40-m w-30-l ma2">
                        <form className="measure center">
                            <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                                <legend className="center f1 fw6 ph0 mh0">Add Lesson</legend>
                                <div >
                                    <a>{this.state.message}</a>
                                </div>

                                <div className="mt3">
                                    <label className="db fw6 lh-copy f3">Lesson Title</label>
                                    <input onChange={this.OninputLessonChange} className="pa2 input-reset ba bw1 b--gold bg-transparent hover-bg-white hover-black w30-m w-80" type="text" name="email-address" id="email-address" />
                                </div>
                                <div className="mt3">
                                    <label className="db fw6 lh-copy f3" >Notes</label>
                                    <textarea rows='5' cols='50' onChange={this.OninputNoteChange} className="pa2 input-reset ba bw1 b--gold bg-transparent hover-bg-white hover-black w-80" type="email" name="email-address" id="email-address" />
                                </div>
                                <div className="mt3">
                                    <label className="db bw2 b--gold fw6 lh-copy f3" >Class Date</label>
                                    <input type='date' onChange={this.OninputDateChange} className="pa2 input-reset ba bg-transparent hover-bg-white hover-black w-99" name="email-address" id="email-address" />
                                    <p>If this is not a class lesson don't select class date</p>
                                </div>
                                <div className="mv3">
                                    <label className="db fw6 lh-copy f3" >Download/Resource Url</label>
                                    <input onChange={this.OninputUrlChange} className="pa2 input-reset ba bw1 b--gold bg-transparent hover-bg-white hover-black w-80" type="text" name="password" id="password" />
                                    <label className="db fw6 lh-copy f3" >Or Upload a Zip file</label>
                                    <input id="myInput"
                                        type="file"
                                        ref={(ref) => this.upload = ref}
                                        onChange={this.onChangeFile.bind(this)} />

                                </div>

                            </fieldset>
                            <div className="">
                                <input onClick={this.saveLesson} className="b center maroon ph3 pv2 input-reset ba br2 bw2 b--gold bg-transparent grow pointer f6 dib" type="submit" value="Save" />
                            </div>
                        </form>
                    </main>


                    <Scroll>
                        <button onClick={() => this.Refresh()}>Refresh</button>
                        <LessonList searchText='' route={route} key={this.state.key} />
                    </Scroll>
                </div>

                <div className="tl bl b--blue bw2 bg-white br3 mb3 black-80 shadow-2 w-100 w-90-m w-50-l ma2 left flex-wrap" >
                    <legend className='center f1 fw6 ph0 mh0'>Push a Notice</legend>
                    <a className="pa2">{this.state.message2}</a>
                    <div className="w-70 center">
                        <br></br>
                        <input onChange={this.OninputNoticeChange} className="pa2 input-reset ba bw1 b--gold bg-transparent hover-bg-white hover-black w-100" type="text" name="notice" id="notice" />
                        <input onClick={this.saveNotice} className="b center maroon  ph3 pv2 input-reset ba br2 bw2 b--gold bg-transparent grow pointer f6 dib" type="submit" value="Save" />
                    </div>

                </div>


                <div className="tl bl b--blue bw2 bg-white br3 mb3 black-80 shadow-2 w-100 w-90-m w-50-l ma2 left flex-wrap" >
                    <legend>Add number of users</legend>
                    <br></br>
                    <div className="w-70 center">
                        <input onChange={this.OnUsercountChange} className="pa2 input-reset ba bw1 b--gold bg-transparent hover-bg-white hover-black w-100" type="number" name="add users" id="add users" />
                        <button onClick={() => { this.addusers(usercount) }}> Add Users</button>
                        
                    </div>
                </div>

            </div>

        )
    }
}

export default Admin;