import React from 'react';

import firebase2 from 'firebase'
import './Register.css'
import { timingSafeEqual } from 'crypto';
import { isRegExp } from 'util';
// var config = {
//   apiKey: "GdzVHQ4E7pvx6R98h7BXLpcar1LRvdSKNcbJQ8c",
//   authDomain: "arduino-class-e854a.firebaseapp.com",
//   databaseURL: "https://arduino-class-e854a.firebaseio.com",
//   projectId: "arduino-class-e854a",
//   storageBucket: "arduino-class-e854a.appspot.com",
//   messagingSenderId: "221258558000"
// };
// firebase2.initializeApp(config);


class Register extends React.Component {
  constructor(props) {
    super();
    this.state = {
      id: '',
      name: '',
      email: '',
      password: '',
      class: '',
      telephone: '',
      message: '',
      registered: false
    }
  }
  addUser = (e) => {
    const { ChangeRoute } = this.props
    if (this.state.name !== "" || this.state.password !== "" || this.state.id !== '') {
      e.preventDefault();
      const db = firebase2.firestore();
      db.collection("users").doc(this.state.id).get()
        .then(doc => {

          if (doc.exists) {
            //registered user
            console.log("doc is exsists")
            if (doc.data().IsRegistered === false){
              console.log("not registered")
              db.collection("users").doc(this.state.id).set({
                name: this.state.name,
                email: this.state.email,
                password: this.state.password,
                class: this.state.class,
                telephone: this.state.telephone,
                IsRegistered: true

              })
            this.setState({
              id: '',
              name: '',
              email: '',
              password: '',
              class: '',
              telephone: '',
              message: 'Done! Successfully registered'
            })
            //delay 
            setTimeout(() => { ChangeRoute("signin") }, 2000)
          }else{
            this.setState({ message: 'ID is already registered' })
          }

          } else {
            this.setState({ message: 'ID is wrong' })
          }
        })

    } else {
      this.setState({ message: 'ID,Name & Passowrd are required' })
    }
  }
  onIDChange = (event) => {
    this.setState({ id: event.target.value })
  }
  onNameChange = (event) => {
    this.setState({ name: event.target.value })
  }
  onEmailChange = (event) => {
    this.setState({ email: event.target.value })
  }
  onPasswordChange = (event) => {
    this.setState({ password: event.target.value })
  }
  onClassChange = (event) => {
    this.setState({ class: event.target.value })
  }
  onTeleChange = (event) => {
    this.setState({ telephone: event.target.value })
  }
  render() {


    return (
      <div className='pt6 '>
        <main className=" pa2 ma2 w-100 w-50-m w-25-l bt b--gold bw2 bg-near-white br3 mb3 black-80 shadow-5 center">
          <form className=" tc measure center">
            <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
              <legend className=" center f1 fw6 ph0 mh0">Register</legend>
              <div>
                <a>{this.state.message}</a>
              </div>
              <div className="mt3">

                <input placeholder="ID" onChange={this.onIDChange} className="br2 pa2 input-reset ba bw1 maroon bg-transparent  w-60" type="text" name="ID" title="Enter the ID provied by admin" id="id" />

              </div>
              <div className="mt3">

                <input placeholder="Name" onChange={this.onNameChange} className="br2 pa2 input-reset ba bw1 maroon bg-transparent  w-60" type="text" name="name" id="name" />
              </div>
              <div className="mt3">

                <input placeholder="E-mail" onChange={this.onEmailChange} className="br2 pa2 input-reset ba bw1 maroon bg-transparent  w-60" type="email" name="email-address" id="email-address" />
              </div>
              <div className="mv3">
                <input placeholder="Create Password" onChange={this.onPasswordChange} className="br2 pa2 input-reset ba bw1 maroon bg-transparent  w-60" type="password" name="password" id="password" />
              </div>
              <div className="mt3">
                <input placeholder="Class" onChange={this.onClassChange} className="br2 pa2 input-reset ba bw1 maroon bg-transparent  w-60" type="text" name="class" id="class" />
              </div>
              <div className="mt3">

                <input placeholder="Telephone Number" onChange={this.onTeleChange} className="br2 pa2 input-reset ba bw1 maroon bg-transparent  w-60" type="number" name="email-address" id="email-address" />
              </div>
            </fieldset>
            <div className="">
              <input onClick={this.addUser} className="b center ph3 pv2 input-reset ba b--gold bw1 bg-transparent br3 grow pointer f6 dib" type="submit" value="Register" />
            </div>
          </form>
        </main>

      </div>
    )
  }
}

export default Register;