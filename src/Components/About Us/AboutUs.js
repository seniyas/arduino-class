import React from 'react'
import seniya from "../About Us/seniya.jpg"
import pamuditha from "../About Us/pamuditha.jpg"
import credits from '../About Us/credits.jpg'

const AboutUs = () => {
    return (
       
        <div className="flex flex-wrap">

            <article class="hover-bg-white bg-animate bg-white-60 mt6 tc center  br3 pa1 pa1-ns ba b--gold w-90 w-80-l bw2 w-80-m">
                    <h1 className="f2 center yellow"> Arduino Morning Class Student Portal</h1>
                    <div className="tl"> 
                        <h3>How to register?</h3>
                        <ol>
                            <li>
                                Click on "Register" in sign in page
                            </li>
                            <li>
                                Enter the ID correctly provied to you in the class.
                            </li>
                            <li>
                                Then enter the other information correctly & click "Register"
                            </li>
                            <li>
                                After that you will automatically directed to the sign in page
                            </li>
                           
                        </ol>
                                                    If somthing not right please feel free to contact us

                    </div>
                   
                    <p><pre>©2019</pre></p>
                    <h4><pre>Designed by</pre></h4>
                   <h4><pre>Seniya Sansith Dissanayake</pre></h4>
                   
            </article>
            <article class=" grow ma4 tc center bg-white br3 pa3 pa4-ns ba b--gold bw1 w-80 w-30-l w-30-m">
                <div class="tc">
                    <img src={seniya} className="br-100 h5 w5 dib ba b--gold pa2" title="Seniya Sansith"></img>
                    <h1 class="f2 mb2 gold">Seniya Sansith</h1>
                    <h2 class="f5 fw4 gray mt0">Tutor</h2>
                    <h3 class="f5 fw4 gray mt0">(+94) 775444169</h3>
                </div>
                <a className="link dim gray dib h2 w2 br-100 mr3 " target="_blank" href="https://www.facebook.com/sansith.dissanayeke" title="">
                    <svg data-icon="facebook" viewBox="0 0 32 32" >
                        <title>facebook icon</title>
                        <path d="M8 12 L13 12 L13 8 C13 2 17 1 24 2 L24 7 C20 7 19 7 19 10 L19 12 L24 12 L23 18 L19 18 L19 30 L13 30 L13 18 L8 18 z"></path>
                    </svg>
                </a>
                <a className="link dim gray dib h2 w2 br-100 mr3 " target="_blank" href="https://twitter.com/Seniya_S" title="">
                    <svg data-icon="twitter" viewBox="0 0 32 32" >
                        <title>twitter icon</title>
                        <path d="M2 4 C6 8 10 12 15 11 A6 6 0 0 1 22 4 A6 6 0 0 1 26 6 A8 8 0 0 0 31 4 A8 8 0 0 1 28 8 A8 8 0 0 0 32 7 A8 8 0 0 1 28 11 A18 18 0 0 1 10 30 A18 18 0 0 1 0 27 A12 12 0 0 0 8 24 A8 8 0 0 1 3 20 A8 8 0 0 0 6 19.5 A8 8 0 0 1 0 12 A8 8 0 0 0 3 13 A8 8 0 0 1 2 4"></path>
                    </svg>
                </a>
                <a class="link dim gray dib br-100 h2 w2 mr3" target="_blank" href="https://www.youtube.com/channel/UC6GveOLbGU4b6ka5BXrbxWg?view_as=subscriber" title="youtube">
                    <svg class="dib w2 h2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><path d="M0 7.345c0-1.294.16-2.59.16-2.59s.156-1.1.636-1.587c.608-.637 1.408-.617 1.764-.684C3.84 2.36 8 2.324 8 2.324s3.362.004 5.6.166c.314.038.996.04 1.604.678.48.486.636 1.588.636 1.588S16 6.05 16 7.346v1.258c0 1.296-.16 2.59-.16 2.59s-.156 1.102-.636 1.588c-.608.638-1.29.64-1.604.678-2.238.162-5.6.166-5.6.166s-4.16-.037-5.44-.16c-.356-.067-1.156-.047-1.764-.684-.48-.487-.636-1.587-.636-1.587S0 9.9 0 8.605v-1.26zm6.348 2.73V5.58l4.323 2.255-4.32 2.24h-.002z" /></svg>

                </a>

            </article>
            <article class=" grow ma4 tc center bg-white br3 pa3 pa4-ns ba b--gold bw1 w-80 w-30-l w-30-m">
                <div class="tc">
                    <img src={pamuditha} class="br-100 h5 w5 dib ba b--gold  pa2" title="Pamuditha"></img>
                    <h1 class="f2 mb2 gold">Pamuditha Premachandra</h1>
                    <h2 class="f5 fw4 gray mt0">Tutor</h2>
                    <h3 class="f5 fw4 gray mt0">(+94) 752731826</h3>
                </div>
                <a className="link dim gray dib h2 w2 br-100 mr3 " target="_blank" href="https://www.facebook.com/pamuditha.next" title="">
                    <svg data-icon="facebook" viewBox="0 0 32 32" >
                        <title>facebook icon</title>
                        <path d="M8 12 L13 12 L13 8 C13 2 17 1 24 2 L24 7 C20 7 19 7 19 10 L19 12 L24 12 L23 18 L19 18 L19 30 L13 30 L13 18 L8 18 z"></path>
                    </svg>
                </a>
                <a class="link dim gray dib br-100 h2 w2 mr3" target="_blank" target="_blank" href="https://www.youtube.com/c/nextcorp?fbclid=IwAR0mv_NcjcvxHVSuEpes6hUBmo0ARjWB7N9u7OS-OhLswsMTZzFzlDvKaVw" title="youtube">
                    <svg class="dib w2 h2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><path d="M0 7.345c0-1.294.16-2.59.16-2.59s.156-1.1.636-1.587c.608-.637 1.408-.617 1.764-.684C3.84 2.36 8 2.324 8 2.324s3.362.004 5.6.166c.314.038.996.04 1.604.678.48.486.636 1.588.636 1.588S16 6.05 16 7.346v1.258c0 1.296-.16 2.59-.16 2.59s-.156 1.102-.636 1.588c-.608.638-1.29.64-1.604.678-2.238.162-5.6.166-5.6.166s-4.16-.037-5.44-.16c-.356-.067-1.156-.047-1.764-.684-.48-.487-.636-1.587-.636-1.587S0 9.9 0 8.605v-1.26zm6.348 2.73V5.58l4.323 2.255-4.32 2.24h-.002z" /></svg>

                </a>

            </article>


        </div>
    )
}

export default AboutUs;