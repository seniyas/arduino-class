import React from 'react'
import 'tachyons'
const Scroll=(props)=>{
return(
    <div className="w-100 w-80-l w-100-m center" style={{overflowY:'scroll',border:'1px ',height:'800px'}}>
            {props.children}
        </div>
        
)
}
export default Scroll;