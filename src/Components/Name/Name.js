import React from 'react';
import 'tachyons';
import firebase from 'firebase'


class Name extends React.Component {

    constructor(props) {
        super()
        this.state = {
            notice: ''
        }
    }
    getNotice = () => {
        const db = firebase.firestore()
        db.collection("users").doc("notice").get()
            .then(doc => {
                if (doc.exists) {

                    this.setState({ notice: doc.data().msg })
                } else {

                }


            })
    }
    componentWillMount() {
        this.getNotice();
    }
    render() {

        const { name, id, OnInputChange } = this.props
        return (

            <div className='flex-wrap  bg-animate  tc w-90 mt6 pa3 bg-white-60 hover-bg-near-white mw6 center br3 shadow-2'>
                <marquee className="red">{this.state.notice}</marquee>
                <div className='black-70 tc'>
                    <a className='f2 '>Hi!,{name}</a>
                    <p className='f4'>ID:{id}</p>
                  
                    <a target="_blank" href="https://drive.google.com/drive/folders/1tnZ5J-DEYoGw_cHi51sBsoLShAddx83D?usp=sharing" className="link dim f4">Arduino PDF Books Collection</a>
                </div>
                <hr></hr>
                <div>
                    <input placeholder="Search Lessons" onChange={OnInputChange} className="f5 br3 pa1 b--maroon w-80 center" type='text' />
                </div>


            </div>
        )

    }
}
export default Name;

