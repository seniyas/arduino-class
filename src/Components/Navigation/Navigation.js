import React from 'react';
import 'tachyons';
import './Navigation.css';
import logo from './ardu team logo_black.png';
const Navigation = ({ route, OnRouteChange }) => {
    return (
        <div>


            <nav className="dt flex-items-start bg-gold w-100 border-box pa1 fixed" >
                <img className="pa2" src={logo} width="80" height="50" alt="ArduTeam Logo"></img>
                <div className="dtc  w-50-l w-50-m   v-mid mid-gray link dim" title="Home" >
                    <p className=" link tc dark-red f6 f3-ns dib">ACICTS:Arduino Class</p>
                </div>


                {route === 'signin'
                    ?
                    // dtc hover-dim v-mid w-10 h-10  tc maroon bw2 br2 b--black 

                    <div className="dtc bg-white-70 bg-animate hover-bg-white  v-mid w-10 h-10 tc maroon br2 ">
                        <a href="#" onClick={() => OnRouteChange("register")} className="pointer ph1 link dim dark-red  dib" title="Register">Register</a>
                    </div>


                    : (route === 'register'
                        ?


                        <div className="dtc bg-white-70 bg-animate hover-bg-white pa2 v-mid w-10 h-10 bg-gold-80  tc maroon br2 ">
                            <a href="#" onClick={() => OnRouteChange("signin")} className="pointer ph1 link dim dark-red  dib" title="Sign In">Sign In</a>
                        </div>

                        : (route === "aboutus")

                            ?
                            <div className="dtc bg-white-70 bg-animate hover-bg-white pa2 v-mid w-10 h-10 bg-gold-80  tc maroon br2 ">
                                <a href="#" onClick={() => OnRouteChange("signin")} className="pointer ph1 link dim dark-red  dib" title="Sign Out">Back</a>
                            </div>



                            :
                            <div className="dtc bg-white-70 bg-animate hover-bg-white pa2 v-mid w-10 h-10 bg-gold-80  tc maroon br2 ">
                                <a href="#" onClick={() => OnRouteChange("signin")} className="pointer ph1 link dim dark-red  dib" title="Sign Out">Sign Out</a>
                            </div>




                    )

                }



            </nav>

        </div >
    )
}

export default Navigation;
