import React, { Component } from 'react'

import Navigation from './Components/Navigation/Navigation'
import './App.css';
import Signin from './Components/Signin/Signin'
import Name from './Components/Name/Name'
import LessonList from './Components/Lesson/lessonlist'
import 'tachyons'
import AboutUs from './Components/About Us/AboutUs'
import Register from './Components/Register/Register'
import Admin from './Components/Admin/Admin'
import Scroll from './Components/Scroll/Scroll';

import { DH_CHECK_P_NOT_PRIME } from 'constants';

// var BackgroundStyle = {

//   backgroundImage: `url(${Background})`,
//   backgroundRepeat: 'no-repeat',
//   backgroundSize:'body',
//   backgroundPosition: 'center',
//   height:"100%"
// }

const backStyle = {
  position: "absolute",
  width: '100%',
  height: "100%",
  left: '0',
  top: '0',
  bottom: '0',
  overflow: "auto",
  background: "rgba(0, 0, 0, 0.438)",
  zindex: "10"
}

class App extends Component {
  constructor() {
    super()
    this.state = {
      route: 'signin',
      search: '',
      name: '',
      id: '',
      notice: ""
    }
  }

  LoadUsers = (user) => {
    this.setState({
      route: 'home',
      search: '',
      name: user.name,
      id: user.id
    })
  }
  ChangeRoute = (route) => {
    this.setState({ route: route });

  }
  OnInputChange = (event) => {
    this.setState({ search: event.target.value });
    // console.log(this.state.search);
  }

  
  render() {
    return (
      <div style={backStyle}>
      <Navigation style={{ position: "sticky" }} route={this.state.route} OnRouteChange={this.ChangeRoute} />
        {this.state.route === 'signin'
          ?
          <div>
            <Signin LoardUsers={this.LoadUsers} ChangeRoute={this.ChangeRoute} />
          </div>
          : (this.state.route === 'home'
            ?
            <div>
              <Name notice={this.getNotice} name={this.state.name} id={this.state.id} OnInputChange={this.OnInputChange} />
              <Scroll>
                <LessonList searchText={this.state.search} />
              </Scroll>
            </div>
            : (this.state.route === 'register')
              ?
              <Register ChangeRoute={this.ChangeRoute} />
              :(this.state.route==="admin")
              ?
              <div>
                <Admin route={this.state.route} />
              </div>
              :
              <div>
                <AboutUs/>

              </div>
            // <div>
            // <Particles style={{ position: "absolute",}} className="particles" params={particleOptions} />
            // </div>
          )
        }
      </div>
    )
  }

}

export default App;
